# DNS Anycast Lab with Docker

## Create an anycast lab that can be deployed in under a minute
 
This git has the files and instructions to create an OSPF Anycast DNS environment in under a minute. 


### Instructions

Docker must be installed on the host system. Instructions to install docker [here](https://docs.docker.com/install/)

        $git clone https://bitbucket.org/rensande/dns-anycast-lab.git
        $cd dns-anycast-lab
        $./build-images
        $./setup
        
### Access the lab

        docker attach client1
        docker attach client2

### Test Anycast

From client 1

        dig @192.168.0.10 infoblox.local
        
From client 2

        dig @192.168.0.10 infoblox.local

To test anycast, stop the OSPF service from one of the DNS servers. 
From the host

        docker attach dns1

From dns1
        
        service quagga stop

Wait for a few minutes and try the above query from client1.


### Delete the environment
        
        $./deletelab
        
### Notes

There is a delay of a few minutes seen after creating the lab for all services on all containers to be loaded . 

Detailed documentaion [here](https://docs.google.com/document/d/1I9__tJIntCqvXAjjM4kVczMGuVirXUwbaSe7sghd_0w/edit?usp=sharing)
